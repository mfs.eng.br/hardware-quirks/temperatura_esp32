#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <InfluxDbClient.h>
#include "config.h"

const char* ssid = WIFI_SSID;
const char* password = WIFI_PASSWORD;

#define DHTPIN 18
#define DHTTYPE DHT22

DHT dht(DHTPIN, DHTTYPE);

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "time-a-g.nist.gov");

InfluxDBClient client(INFLUXDB_URL, INFLUXDB_DB_NAME);

Point sensor("clima");

void setup() {
  Serial.begin(9600);
  
  dht.begin();
  
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(2500);
    Serial.println("Conectando à rede WiFi...");
  }
  Serial.println("Conectado à rede WiFi");
  
  timeClient.begin();
  timeClient.setTimeOffset(-3*60*60); // Ajuste para o fuso horário de -3 horas (Brasil)

  // Configuração do cliente InfluxDB
  client.setConnectionParamsV1(INFLUXDB_URL, INFLUXDB_DB_NAME, INFLUXDB_USER, INFLUXDB_PASSWORD);
}

void loop() {
  timeClient.update();

  float tempValue = dht.readTemperature();
  float humidityValue = dht.readHumidity();

  unsigned long timestamp = timeClient.getEpochTime();
  time_t timeObj = timestamp;
  struct tm* timeinfo = localtime(&timeObj);

  char dataFormatada[20];
  strftime(dataFormatada, sizeof(dataFormatada), "%d/%m/%Y", timeinfo);
  
  if (!isnan(tempValue) && !isnan(humidityValue)) {
    // Limpa os campos do ponto de dados
    sensor.clearFields();

    // Adiciona os campos ao ponto de dados
    sensor.addField("temperature", tempValue);
    sensor.addField("humidity", humidityValue);

    // Adiciona a timestamp ao ponto de dados
    //sensor.setTime(timestamp);

    // Escreve o ponto de dados no InfluxDB
    if (!client.writePoint(sensor)) {
      Serial.print("InfluxDB write failed: ");
      Serial.println(client.getLastErrorMessage());
    }

    // Escreve os dados no serial com o log de data
    Serial.print(dataFormatada);
    Serial.print(" ");
    Serial.print(timeClient.getFormattedTime());
    Serial.print(" - Temperatura: ");
    Serial.print(tempValue);
    Serial.print(" °C, Umidade: ");
    Serial.print(humidityValue);
    Serial.println(" %");
  }
  
  delay(60000); // Aguarda 60 segundos antes da próxima leitura
}
