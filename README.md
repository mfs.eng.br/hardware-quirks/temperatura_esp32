# Projeto de Monitoramento de Temperatura e Umidade com ESP32

Este projeto utiliza um ESP32 para coletar dados de temperatura e umidade de um sensor DHT22 e enviá-los para um banco de dados InfluxDB. Os dados podem ser visualizados através do Grafana.

## Configuração

Para manter os dados sensíveis seguros, este projeto utiliza um arquivo `config.h` para armazenar informações como credenciais de Wi-Fi e do InfluxDB. Você precisará criar seu próprio arquivo `config.h` com suas informações específicas.

### Criando o arquivo `config.h`

1. Na pasta `src` do projeto, crie um novo arquivo chamado `config.h`.
2. Abra o arquivo `config.h` e adicione as seguintes linhas, substituindo os valores pelos seus próprios dados:

```cpp
#define WIFI_SSID "seu_SSID_wifi"
#define WIFI_PASSWORD "sua_senha_wifi"
#define INFLUXDB_URL "http://seu_servidor_influxdb:8086"
#define INFLUXDB_DB_NAME "nome_do_banco_de_dados"
#define INFLUXDB_USER "usuario_influxdb"
#define INFLUXDB_PASSWORD "senha_influxdb"
```

3. Salve o arquivo `config.h`.


## Uso do Projeto

1. Certifique-se de ter criado corretamente o arquivo `config.h` com suas informações.
2. Compile e faça o upload do código para o seu ESP32.
3. O ESP32 irá se conectar à rede Wi-Fi especificada e começar a coletar dados de temperatura e umidade.
4. Os dados serão enviados para o banco de dados InfluxDB em intervalos regulares.
5. Você pode visualizar e analisar os dados usando o Grafana ou outra ferramenta compatível com o InfluxDB.

Se você encontrar algum problema ou tiver alguma dúvida, sinta-se à vontade para abrir uma issue neste repositório.